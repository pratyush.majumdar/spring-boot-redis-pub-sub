package com.pratyush.redis.pubsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
	private int orderId;
	private String productName;
	private int quantity;
	private float price;
	private String address;
}
