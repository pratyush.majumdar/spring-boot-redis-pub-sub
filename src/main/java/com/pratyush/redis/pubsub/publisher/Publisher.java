package com.pratyush.redis.pubsub.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pratyush.redis.pubsub.model.Order;

@RestController
public class Publisher {

	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	ChannelTopic channelTopic;
	
	@PostMapping("/publish")
	public String publish(@RequestBody Order order) {
		redisTemplate.convertAndSend(channelTopic.getTopic(), order.toString());
		return "Order Placed !!!";
	}
}
