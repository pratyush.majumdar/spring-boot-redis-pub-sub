# Spring Boot Redis Pub Sub Implementation
Publish–Subscribe is a messaging pattern where publishers are not programmed to send their messages to specific subscribers. Rather, published messages are characterized into channels, without knowledge of what (if any) subscribers there may be. Subscribers express interest in one or more channels, and only receive messages that are of interest, without knowledge of what (if any) publishers there are. This decoupling of publishers and subscribers can allow for greater scalability and a more dynamic network topology.

## Maven dependencies used
```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<dependency>
	<groupId>redis.clients</groupId>
	<artifactId>jedis</artifactId>
</dependency>
```

## Redis Download Location
[https://redis.io/download](https://redis.io/download)

## Rest API to publish a message to the Topic
http://localhost:8080/publish

```json
{
    "orderId" : 123,
	"productName" : "Books",
	"quantity" : 2,
	"price" : 2000.00,
    "address" : "New Delhi"
}
```

### Build the Project skipping Tests
```mvn
mvn clean install -DskipTests
```

### Run the Project
```mvn
java -jar spring-boot-redis-pub-sub-0.0.1-SNAPSHOT.jar
```
